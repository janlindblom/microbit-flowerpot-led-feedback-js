let light2 = 0
let moisture: number[] = []
let global_val = 0
let light1 = 0
let global_led = 0
let lights = false

lights = true
light1 = 0
light2 = 0
moisture = [0, 0, 0]
radio.setTransmitPower(1)
radio.setGroup(67)

function light_leds() {
    if (lights) {
        // Really dry
        //
        // Moderately dry
        if (global_val < 250) {
            light1 = 0
            light2 = 1023
        } else if (global_val < 500) {
            light1 = 1023
            light2 = 0
        } else {
            light1 = 0
            light2 = 0
        }
        if (global_led == 0) {
            pins.analogWritePin(AnalogPin.P8, light1)
            pins.analogWritePin(AnalogPin.P12, light2)
        } else if (global_led == 1) {
            pins.analogWritePin(AnalogPin.P13, light1)
            pins.analogWritePin(AnalogPin.P14, light2)
        } else if (global_led == 2) {
            pins.analogWritePin(AnalogPin.P15, light1)
            pins.analogWritePin(AnalogPin.P16, light2)
        }
    } else {
        pins.analogWritePin(AnalogPin.P8, 0)
        pins.analogWritePin(AnalogPin.P12, 0)
        pins.analogWritePin(AnalogPin.P13, 0)
        pins.analogWritePin(AnalogPin.P14, 0)
        pins.analogWritePin(AnalogPin.P15, 0)
        pins.analogWritePin(AnalogPin.P16, 0)
    }
}
function show_status() {
    for (let i = 0; i <= 2; i++) {
        global_led = i
        global_val = moisture[i]
        light_leds()
    }
}
radio.onDataPacketReceived( ({ receivedString: name, receivedNumber: value }) =>  {
    if (name == "pot1") {
        moisture[0] = value
    } else if (name == "pot2") {
        moisture[1] = value
    } else if (name == "pot3") {
        moisture[2] = value
    } else if (name == "screen") {
        if (value == 0) {
            lights = false
        } else {
            lights = true
        }
    }
    show_status()
    if (lights) {
        led.plot(0, 4)
        basic.pause(500)
        led.unplot(0, 4)
    }
})

show_status()
