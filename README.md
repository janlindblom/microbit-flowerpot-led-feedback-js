# led-feedback

[![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/janlindblom/microbit-flowerpot-led-feedback-js.png)](https://bitbucket.org/janlindblom/microbit-flowerpot-led-feedback-js)

Light up LEDs based on moisture level measured in a flower pot.

## TODO

- [ ] Add a reference for your blocks here
- [ ] Add "icon.png" image (300x200) in the root folder
- [ ] Add "- beta" to the GitHub project description if you are still iterating it.
- [ ] Turn on your automated build on https://travis-ci.org
- [ ] Use "pxt bump" to create a tagged release on GitHub
- [ ] Get your package reviewed and approved https://makecode.microbit.org/packages/approval

Read more at https://makecode.microbit.org/packages/build-your-own

## License

MIT

## Supported targets

* for PXT/microbit
(The metadata above is needed for package search.)

